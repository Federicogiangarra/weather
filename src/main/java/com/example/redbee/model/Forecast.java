package com.example.redbee.model;

public class Forecast {
    private String day;
    private String description;
    private Integer high;
    private Integer low;

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
            switch(day){
                case "Tue":
                    this.day = day + "sday";
                    break;
                case "Wed":
                    this.day = day + "nesday";
                    break;
                case "Thu":
                    this.day = day + "rsday";
                    break;
                case "Sat":
                    this.day = day + "urday";
                    break;
                default:
                    this.day = day + "day";
                    break;
            }
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getHigh() {
        return high;
    }

    public void setHigh(Integer high) {
        this.high = high;
    }

    public Integer getLow() {
        return low;
    }

    public void setLow(Integer low) {
        this.low = low;
    }
}

package com.example.redbee.services;

import com.example.redbee.model.Board;
import com.example.redbee.model.Location;
import com.example.redbee.repositories.BoardRepository;
import com.example.redbee.repositories.LocationRepository;
import com.example.redbee.scheduler.WeatherScheduler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("boardService")
public class BoardServiceImpl implements BoardService {

    @Autowired
    private BoardRepository repository;

    public Board findByName(String name) {
        return repository.findByName(name);
    }

    @Override
    public void saveBoard(Board board) {
        repository.save(board);
    }

    @Override
    public List<Board> findAllBoards() {
        return repository.findAll();
    }
}
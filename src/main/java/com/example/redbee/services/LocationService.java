package com.example.redbee.services;

import com.example.redbee.model.Location;

import java.util.List;

public interface LocationService {

    List<Location> findByName(String name);

    Location saveLocation(Location location);


    List<Location> findAllLocations();

    Location getForecast(String city);




}

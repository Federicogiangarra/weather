package com.example.redbee.services;

import com.example.redbee.model.Board;
import com.example.redbee.model.Forecast;
import com.example.redbee.repositories.BoardRepository;
import com.example.redbee.repositories.LocationRepository;
import com.example.redbee.model.Location;
import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import java.util.Iterator;
import java.util.List;

@Service("locationService")
public class LocationServiceImpl implements LocationService {

    @Autowired
    private LocationRepository locationRepository;

    @Autowired
    private BoardRepository boardRepository;


    public Location saveLocation(Location location) {

        return locationRepository.save(location);
    }

    public List<Location> findByName(String name) {

        return locationRepository.findByName(name);
    }


    public List<Location> findAllLocations() {

        return locationRepository.findAll();
    }

    public Location getForecast(String cityTemp) {
        Location locationTemp = new Location();
        String cityForLink = cityTemp.replace(" ", "%20").toLowerCase();
        String URI = "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22" + cityForLink + "%2C%20ak%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys";

        ResteasyClient client = new ResteasyClientBuilder().build();
        ResteasyWebTarget target = client.target(URI);
        Response response = target.request().get();

        String value = response.readEntity(String.class);

        JSONObject jsonObject = null;

        try {
            jsonObject = new JSONObject(value);

            //Obtengo el clima a partir del JSON
            JSONObject query = new JSONObject(jsonObject.get("query").toString());
            JSONObject results = new JSONObject(query.get("results").toString());
            JSONObject channel = new JSONObject(results.get("channel").toString());
            JSONObject loc = new JSONObject(channel.get("location").toString());
            String realCity = loc.getString("city");
            JSONObject item = new JSONObject(channel.get("item").toString());
            JSONObject atmo = new JSONObject(channel.get("atmosphere").toString());
            JSONObject condition = new JSONObject(item.get("condition").toString());
            JSONArray forecast = (JSONArray) item.get("forecast");

            locationTemp.setName(realCity);
            locationTemp.setDescription(condition.getString("text"));
            locationTemp.setTemp(Integer.valueOf(condition.getString("temp")));

            List<Forecast> forecasts = locationTemp.getForecasts();

            for (int i = 0; i < forecast.length(); i++) {
                Forecast f = new Forecast();
                JSONObject jsonobj = forecast.getJSONObject(i);
                f.setDay(jsonobj.getString("day"));
                f.setDescription(jsonobj.getString("text"));
                f.setHigh(Integer.valueOf(jsonobj.getString("high")));
                f.setLow(Integer.valueOf(jsonobj.getString("low")));
                forecasts.add(f);
            }
            return locationTemp;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    // Every 1 minute the app will refresh the locations
    @Scheduled(fixedRate = 60000)
    public void getUpdatedLocations(){
        List<Location> locations = this.findAllLocations();
        Location l = null;
        for (int i = 0; i < locations.size() ; i++) {
            l = locations.get(i);
            Location location = this.getForecast(locations.get(i).getName());
            locations.get(i).setTemp(location.getTemp());
            locations.get(i).setDescription(location.getDescription());
            locations.get(i).setForecasts(location.getForecasts());
            this.saveLocation(locations.get(i));
        }
    }
}

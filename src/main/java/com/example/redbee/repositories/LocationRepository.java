package com.example.redbee.repositories;

import com.example.redbee.model.Location;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface LocationRepository extends MongoRepository<Location, String> {
        public List<Location> findByName(String name);
        public List<Location> findAll();
}

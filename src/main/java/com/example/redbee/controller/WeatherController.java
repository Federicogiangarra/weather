package com.example.redbee.controller;

import com.example.redbee.dto.BoardDTO;
import com.example.redbee.model.Board;
import com.example.redbee.services.BoardService;
import com.example.redbee.services.LocationService;
import com.example.redbee.model.Location;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class WeatherController {

    @Autowired
    private BoardService boardService;

    @Autowired
    private LocationService locationService;

    private String message = "Welcome to our App";

    @RequestMapping(value = {"/", "/index"}, method = RequestMethod.GET)
    public String index(Model model) {

        model.addAttribute("message", message);
        return "index";
    }

    // Endpoint to check with PostMan
    @RequestMapping("/locations/{name}")
    @ResponseBody
    public List<Location> getLocationByName(@PathVariable("name") String name) {
        return locationService.findByName(name);
    }

    // Endpoint to check with PostMan
    @RequestMapping("/locations")
    @ResponseBody
    public List<Location> getLocations() {
        return locationService.findAllLocations();
    }

    // Endpoint to check with PostMan
    @PostMapping("/boards2")
    public ResponseEntity<String> addBoard(@RequestBody Board board) {
        boardService.saveBoard(board);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    // Endpoint to check with PostMan
    @RequestMapping("/boards")
    @ResponseBody
    public List<Board> getBoards() {

        return boardService.findAllBoards();
    }

    @RequestMapping(value = "/boards", method = RequestMethod.POST)
    public String addNewBoard(Model model, @ModelAttribute("board") BoardDTO boardDTO, @ModelAttribute("location") Location location) {
        String message = "";
        String errorMessage = "";
        String boardsName = boardDTO.getName().substring(0, boardDTO.getName().indexOf(","));
        String locationsName = boardDTO.getName().substring(boardDTO.getName().indexOf(",") + 1, boardDTO.getName().length());

        Location locationWithForecast = locationService.getForecast(locationsName);

        Board boardFromDB = boardService.findByName(boardsName);

        Location l = locationService.saveLocation(locationWithForecast);

        if (!locationsName.equals(locationWithForecast.getName())) {
            message = "The location " + locationsName + " does not exist. The location " + locationWithForecast.getName() + " was added.";
        } else {
            message = "The location " + locationWithForecast.getName() + " was added.";
        }

        if (boardFromDB == null) {
            Board newBoard = new Board();
            newBoard.setName(boardsName);
            newBoard.getLocations().add(l);

            boardService.saveBoard(newBoard);
            model.addAttribute("board", newBoard);

            message = message + " The board " + newBoard.getName() + " was created.";
            model.addAttribute("message", message);
        } else {
            boardFromDB.getLocations().add(l);
            boardService.saveBoard(boardFromDB);
            model.addAttribute("board", boardFromDB);
            errorMessage = "The board " + boardFromDB.getName() + " already exists.";
            model.addAttribute("message", message);
        }
        model.addAttribute("location", l);
        return "index";
    }


    @RequestMapping("/newBoard")
    public String showAddPersonPage(Model model) {

        Board board = new Board();
        model.addAttribute("board", board);

        Location location = new Location();
        model.addAttribute("location", location);

        return "newBoard";
    }

    @RequestMapping("/unMapLocationToBoard")
    public String unMapLocationToBoard(Model model) {

        BoardDTO boardDTO = new BoardDTO();
        model.addAttribute("board", boardDTO);

        Location location = new Location();
        model.addAttribute("location", location);

        return "unMapLocationToBoard";
    }


    @RequestMapping("/showLocationFromBoard")
    public String showLocationFromBoard(Model model) {
        Board board = new Board();
        model.addAttribute("board", board);

        Location location = new Location();
        model.addAttribute("location", location);

        return "showLocationFromBoard";
    }

    @RequestMapping(value = "/searchLocationsFromBoard", method = RequestMethod.POST)
    public String serchLocationsFromBoard(Model model, @ModelAttribute("board") BoardDTO boardDTO) {
        String errorMessage = "";

        Board boardFromDB = boardService.findByName(boardDTO.getName());
        if (boardFromDB != null) {
            List<Location> locations = boardFromDB.getLocations();
            model.addAttribute("boardFromDB", boardFromDB);
        } else {
            errorMessage = "Board does not exist";
            model.addAttribute("errorMessage", errorMessage);
        }
        return "listAllLocations";
    }

    @RequestMapping(value = "/unmapLocation", method = RequestMethod.POST)
    public String unmapLocation(Model model, @ModelAttribute("board") BoardDTO boardDTO, @ModelAttribute("location") Location location) {

        String boardsName = boardDTO.getName().substring(0, boardDTO.getName().indexOf(","));
        String locationsName = boardDTO.getName().substring(boardDTO.getName().indexOf(",") + 1, boardDTO.getName().length());
        String message = "";
        String errorMessage = "";
        int size = 0;

        Board boardFromDB = boardService.findByName(boardsName);

        if (boardFromDB != null) {
            List<Location> locations = boardFromDB.getLocations();

            size = locations.size();

            for (int i = 0; i < locations.size(); i++) {
                Location aux = locations.get(i);
                if (aux.getName().equals(locationsName)) {
                    boardFromDB.getLocations().remove(i);
                    boardService.saveBoard(boardFromDB);
                }
            }

            if (locations.size() != size) {
                System.out.println("Location was removed");
                message = "Location was removed";
                model.addAttribute("message", message);
            } else {
                System.out.println("Location was not removed");
                errorMessage = "Location was not removed";
                model.addAttribute("errorMessage", errorMessage);
            }
        } else { // Board does not exist
            errorMessage = "Board does not exist";
            model.addAttribute("errorMessage", errorMessage);
        }
        return "index";
    }
}

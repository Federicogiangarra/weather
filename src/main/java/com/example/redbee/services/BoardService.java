package com.example.redbee.services;

import com.example.redbee.model.Board;
import com.example.redbee.model.Location;

import java.util.List;

public interface BoardService {
    Board findByName(String name);

    void saveBoard(Board board);

    List<Board> findAllBoards();
}

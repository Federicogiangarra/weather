package com.example.redbee.repositories;

import com.example.redbee.model.Board;
import com.example.redbee.model.Location;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface BoardRepository extends MongoRepository<Board, String> {
    public Board findById(String id);
    public Board findByName(String name);
    public List<Board> findAll();
}
